//Require Firebase libraries
const admin = require('firebase-admin');
const functions = require('firebase-functions');

//Require Actions on Google for Dialogflow Fulfillments
const {
    dialogflow,
    SignIn,
} = require('actions-on-google');

//Initialize Firebase Admin
admin.initializeApp(functions.config().firebase);
const db = admin.firestore();

//Initialize Dialogflow
const app = dialogflow({
    clientId: "678393516073-57p6tl9ba865es387jm4vq2benonrb3i.apps.googleusercontent.com",
    debug: true
});


//Firebase functions begin

//Add data to database on user creation
exports.addUserDataOnCreation = functions.auth.user().onCreate((user) => {
    //Perform query
    db.collection("users").doc(user.uid).set({
        displayName: user.displayName,
        email: user.email,
        photoURL: user.photoURL,
        rank: 1
    })
        .then(function() {
            console.log("User " + user.email + " created!");
        })
        .catch(function(error) {
            console.error("Error writing document: ", error);
        });
    return 0;
});



//Dialogflow Fulfillments begin

//Get temperature
app.intent('Get Temperature', async (conv, signin) => {
    //Check if user is signed in
    const payload = conv.user.profile.payload;
    if (payload) {
        //Get room parameter
        const roomName = conv.parameters['room'].toLowerCase();
        //Get user ID from Email
        userId = await getUserIdFromUserEmail(conv.user.email)
        //Get rooms
        userRooms = await getRoomNamesFromUserId(userId)
        //Make room array lowercase for comparison
        for (let i = 0; i < userRooms.length; i++) {
            userRooms[i] = userRooms[i].toLowerCase();
        }
        //Check if room exists for user
        if (userRooms.includes(roomName)) {
            //Get Room ID
            const roomId = await getRoomIdFromRoomName(roomName)
            //Get Temperature
            const roomTemperature = await getRoomTempFromRoomId(roomId)
            //Return tts information
            conv.ask(`Es sind ${roomTemperature} Grad im Raum ${roomName}`);
        } else {
            //Room not found
            conv.ask(`Ich habe den Raum ${roomName} nicht gefunden!`);
        }
    } else {
        //Start sign in
        conv.ask(new SignIn('Damit ich dein Zuhause steuern kann'));
    }
});



//Helper functions here
async function getUserIdFromUserEmail(userEmail) {
    const querySnapshot = await db.collection("users").where("email", "==", userEmail).limit(1).get();
    const ids = querySnapshot.docs.map((doc) => { return doc.id });
    return ids[0];
}

async function getRoomNamesFromUserId(userId) {
    const querySnapshot = await db.collection("rooms").where("holder", "==", userId).get();
    const names = querySnapshot.docs.map((doc) => { return doc.data().name });
    return names;
}

async function getRoomIdFromRoomName(roomName) {
    const querySnapshot = await db.collection("rooms").where("name", "==", roomName).limit(1).get();
    const ids = querySnapshot.docs.map((doc) => { return doc.id });
    return ids[0];
}

async function getRoomTempFromRoomId(roomId) {
    const querySnapshot = await db.collection("sensors").where("room", "==", roomId).where("type", "==", "temperature").limit(1).get();
    const values = querySnapshot.docs.map((doc) => { return doc.data().value });
    return values[0];
}



//Export fulfillments
exports.fulfillment = functions.https.onRequest(app);