firebase.auth().onAuthStateChanged(function(user) {
    if (user) {

        db.collection("rooms").where("holder", "==", user.uid)
        .onSnapshot(function(snapshot) {
            snapshot.docChanges().forEach(function(change) {
                if (change.type === "added") {
                    renderRooms(change.doc.id, change.doc.data().name)
                }
            });
        });
    }
});


function renderRooms(id, name) {
    var table = document.getElementById("rooms");

    let tr = document.createElement('tr');

    let button = document.createElement('button');
    let room = document.createElement('td');
    let view = document.createElement('td');

    
    button.textContent = "View";
    room.textContent = name;

    button.setAttribute("onclick", "window.location.href=\"view.html?id=" + id + "\"" )

    view.appendChild(button)
    tr.appendChild(room);
    tr.appendChild(view);
    table.appendChild(tr)

}
