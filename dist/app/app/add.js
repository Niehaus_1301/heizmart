//Fetch URL
var url_string = window.location.href
var url = new URL(url_string);

//Define parameter variables
var id = url.searchParams.get("id");
var room = url.searchParams.get("room");

//Select room Select
var selectList = document.getElementById("rooms");

//Get user object
firebase.auth().onAuthStateChanged(function(user) {
    //Authenticated actions here

    //Query rooms once
    db.collection("rooms").where("holder", "==", user.uid)
    .get()
    .then(function(querySnapshot) {
        //Check if no rooms
        querySnapshot.forEach(function(doc) {
            //Check if room already has a sensor registered
            db.collection("sensors").where("room", "==", doc.id)
            .get()
            .then(snap => {
                if (snap.size == 0) {
                    //Render room
                    renderRooms(doc.id, doc.data().name)
                }
            });
        });
        //Preselect after document ready
        preSelect()
    })
    .catch(function(error) {
        //Report error
        console.log("Error getting documents: ", error);
        document.getElementById("alert").innerHTML = "Fehler!"
    });
});

function preSelect() {
    //Check if parameter exists and set element value
    if (id) {
    document.getElementById("id").value = id;
    }
    if (room) {
        document.getElementById("rooms").value = room;
    }
}



function renderRooms(id, name) {
    //Add option with room value
    var option = document.createElement("option");
    option.value = id;
    option.text = name;
    selectList.appendChild(option);
}


function validate() {
    //Get user object
    firebase.auth().onAuthStateChanged(function(user) {
        //Get field values
        id = document.getElementById("id").value;
        room = document.getElementById("rooms").value;

        //Check if empty
        if (id == "") {
            document.getElementById("alert").innerHTML = "Eingabe erforderlich"
        } else {
            //Set sensors docRef
            var docRef = db.collection("sensors").doc(id);
            //Query sensors once
            docRef.get().then(function(doc) {
                if (doc.exists) {
                    //Check if already registered
                    if (typeof doc.data().holder == 'undefined') {
                        //Update with new details
                        return docRef.update({
                            holder: user.uid,
                            room: room
                        })
                        .then(function() {
                            //Forward user to room view
                            window.location.href = "view.html?id=" + room
                        })
                        .catch(function(error) {
                            //Report error
                            console.log("Error updating documents: ", error);
                            document.getElementById("alert").innerHTML = "Fehler!"
                        });
                    } else {
                        //Already registered
                        document.getElementById("alert").innerHTML = "Sensor bereits registriert!"
                    }
                } else {
                    //Not found in database
                    document.getElementById("alert").innerHTML = "Sensor unbekannt"
                }
            }).catch(function(error) {
                //Report error
                console.log("Error getting documents: ", error);
                document.getElementById("alert").innerHTML = "Fehler!"
            });
        }
    });
}

function checkForSensor(room) {
    
}